package ro.tuc.ds2020.listener;


import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Component;
import ro.tuc.ds2020.entities.Measure;
import ro.tuc.ds2020.entities.Senzor;
import ro.tuc.ds2020.repositories.MeasureRepository;
import ro.tuc.ds2020.repositories.SenzorRepository;

@Component
public class MeasurementListener {

    @Autowired
    private SimpMessagingTemplate template;

    private final MeasureRepository measureRepository;
    private final SenzorRepository senzorRepository;

    public MeasurementListener(SenzorRepository senzorRepository, MeasureRepository measureRepository) {
        this.measureRepository = measureRepository;
        this.senzorRepository = senzorRepository;
    }

    @RabbitListener(queues = MQConfig.QUEUE)
    public void listener(MeasurementMessage measurementMessage) {

        Measure[] measurementsVect = measureRepository.findAll().toArray(new Measure[0]);
        Senzor senzor = senzorRepository.findById(measurementMessage.getSensor_id()).get();

        Measure measure = new Measure(null, measurementMessage.getDateTime(), measurementMessage.getValue(), senzor);
        if (senzor.getMaxValue() >= (measure.getValue() - measurementsVect[measurementsVect.length - 1].getValue()) ) {
            template.convertAndSend("/topic/socket/measure/measuree", measurementMessage );
            measureRepository.save(measure);

        }
        else {

            template.convertAndSend("/topic/socket/measure/msg",
                    "Id masuratoare: " + (measurementsVect[measurementsVect.length-1].getId()) + ", timp: " + measure.getDateTime() + ", consum: " +measure.getValue() + "\n - Id masuratoare: " +
                            measurementsVect[measurementsVect.length - 1].getId() + ", timp: "+
                            measurementsVect[measurementsVect.length - 1].getDateTime() + ", consum: "+
                            measurementsVect[measurementsVect.length - 1].getValue() + ", mai mare ca " + senzor.getMaxValue() );
            return;
        }
    }

}
