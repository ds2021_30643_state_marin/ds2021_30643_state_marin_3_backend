package ro.tuc.ds2020.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import ro.tuc.ds2020.entities.Measure;

import java.util.List;
import java.util.UUID;

public interface MeasureRepository extends JpaRepository<Measure, UUID> {
    @Query(value = "SELECT m " +
            "FROM Measure m INNER JOIN Senzor s on " +
            "m.senzor.id = s.id WHERE s.id = :senzorId")
    List<Measure> findMeasurementsBySenzorId(UUID senzorId);
}
