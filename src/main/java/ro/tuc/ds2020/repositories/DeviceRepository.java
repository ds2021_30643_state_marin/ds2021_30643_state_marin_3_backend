package ro.tuc.ds2020.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import ro.tuc.ds2020.entities.Device;

import javax.transaction.Transactional;
import javax.validation.Valid;
import java.util.List;
import java.util.UUID;

public interface DeviceRepository extends JpaRepository<Device, UUID> {
    @Query(value = "SELECT d " +
            "FROM Device d INNER JOIN Client c on " +
            "d.client.id = c.id WHERE c.id = :id")
    List<Device> findDevicesById(@Param("id") UUID id);
    @Transactional
    @Modifying
    @Query(value = "UPDATE Device d " +
            "set d.client.id = null " +
            "where d.client.id = :id")
    void updateDeviceIdAfterDelete(@Param("id") UUID id);
    @Transactional
    @Modifying
    @Query(value = "UPDATE Device d " +
            "set d.senzor.id = null " +
            "where d.id = :deviceId")
    void updateSenzorIdAfterDelete(UUID deviceId);

    @Transactional
    @Modifying
    @Query(value = "UPDATE Device d " +
            "set d.senzor.id = null " +
            "where d.senzor.id = :id")
    void updateDeviceAfterSenzorDelete(UUID id);
}
