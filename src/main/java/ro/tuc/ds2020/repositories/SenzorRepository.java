package ro.tuc.ds2020.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import ro.tuc.ds2020.entities.Senzor;

import javax.transaction.Transactional;
import java.util.Optional;
import java.util.UUID;

public interface SenzorRepository extends JpaRepository<Senzor, UUID> {
    @Query(value = "SELECT s " +
            "FROM Senzor s INNER JOIN Device d on " +
            "d.senzor.id = s.id WHERE d.id = :deviceId")
    Optional<Senzor> findSenzorByDeviceId(UUID deviceId);

}
