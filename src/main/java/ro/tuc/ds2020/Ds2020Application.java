package ro.tuc.ds2020;


import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.annotation.Bean;
import org.springframework.validation.annotation.Validated;
import ro.tuc.ds2020.entities.*;
import ro.tuc.ds2020.repositories.*;

import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.*;

@SpringBootApplication
@Validated
public class Ds2020Application extends SpringBootServletInitializer {
    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
        return application.sources(Ds2020Application.class);
    }

    public static void main(String[] args) {

		TimeZone.setDefault(TimeZone.getTimeZone("UTC"));
        SpringApplication.run(Ds2020Application.class, args);


    }
    @Bean
    CommandLineRunner init(ClientRepository clientRepository, AdministratorRepository administratorRepository,
                           DeviceRepository deviceRepository, MeasureRepository measureRepository,
                           SenzorRepository senzorRepository){
        return args -> {

//            Administrator admin = new Administrator(null,"andreea","andreea","123");
//            administratorRepository.save(admin);
//
//
//            Measure measure = new Measure(LocalDateTime.now(),100f);
//            Measure measure2 = new Measure(LocalDateTime.now(),200f);
//            Measure measure3 = new Measure(LocalDateTime.now(),100f);
//            Measure measure4 = new Measure(LocalDateTime.now(),200f);
//            List<Measure> measureList = new ArrayList<Measure>();
//            List<Measure> measureList2 = new ArrayList<Measure>();
//
//
//            Senzor senzor = new Senzor(null,"Senzor Smart house",200f,measureList);
//            Senzor senzor2 = new Senzor(null,"Senzor Smart house2",200f,measureList2);
//            senzor.setMeasureList(measureList);
//            senzor2.setMeasureList(measureList2);
//            senzorRepository.save(senzor);
//            senzorRepository.save(senzor2);
//
//            measure.setSenzor(senzor);
//            measure2.setSenzor(senzor);
//            measure3.setSenzor(senzor2);
//            measure4.setSenzor(senzor2);
//            measureList.add(measure);
//            measureList.add(measure2);
//            measureList2.add(measure3);
//            measureList2.add(measure4);
//
//            measureRepository.saveAll(measureList);
//            measureRepository.saveAll(measureList2);
//
//            Device device =new Device("Smart house","Cluj",100f,50f,senzor);
//            Device device2 =new Device("Smart house2","Cluj",200f,100f,senzor2);
//            deviceRepository.save(device);
//            deviceRepository.save(device2);
//            senzor.setDevice(device);
//            senzor2.setDevice(device2);
//            senzorRepository.save(senzor);
//            senzorRepository.save(senzor2);
//            List<Device> deviceList = new ArrayList<Device>();
//
//
//            Client client = new Client(null, "Marin", "marin","123", "Cluj",
//                    new Date(),deviceList);
//
//            clientRepository.save(client);
//
//            device.setClient(client);
//            device2.setClient(client);
//            deviceList.add(device);
//            deviceList.add(device2);
//            deviceRepository.saveAll(deviceList);
//            System.out.println(client.getId());
//            Date date = new Date(1999,12,12);
//            System.out.println(date.toString().substring(11,13));
//            System.out.println(senzor.getId());

          //  List<Client> clientList =  clientRepository.findAll();

          //  List<Senzor> list = senzorRepository.findAll();
          //  float nr = 3;
          //  for(Senzor senzor : list){
          //      senzor.setMaxValue(nr);
           //     senzorRepository.save(senzor);
           //     nr+=2;
          //  }

//            List<Client>  clientList= clientRepository.findAll();
//            for(Client client : clientList){
//                System.out.println(client.getId());
//            }


            LocalDateTime date = LocalDateTime.now();

            List<Measure> measureList = new ArrayList<>();
            List<Device> deviceList = deviceRepository.findDevicesById(UUID.fromString("0de0d761-5b24-4caf-a6f8-0c64d5691f05"));
            List<Senzor> senzorList = new ArrayList<>();
            Float[][] valueMat = new Float[3][24];
            for(int i = 0 ; i< 3; i++){
                for(int j = 0; j<24; j++){
                    valueMat[i][j] = 0f;
                }
            }

            for(Device device: deviceList){
                Senzor senzor = device.getSenzor();
                senzorList.add(senzor);
            }
            for(Senzor senzor:senzorList){
                if (senzor.getMeasureList() != null)
                    for(Measure measure : senzor.getMeasureList()){

                        measureList.add(measure);
                    }
            }
            Collections.sort(measureList, new Comparator<Measure>() {
                public int compare(Measure o1, Measure o2) {
                    return o2.getDateTime().compareTo(o1.getDateTime());
                }
            });
            int k =0;
            for (int i = 0; i< measureList.size(); i++){
                System.out.println("Toate masuratorile : " + measureList.get(i).getDateTime().toString());
                //k contor pana la d
                //j ora
                if(measureList.get(i).getDateTime().getMonth().getValue() < date.getMonth().getValue() || ((measureList.get(i).getDateTime().getMonth() == date.getMonth()) &&
                        (measureList.get(i).getDateTime().getDayOfMonth() < date.getDayOfMonth()))){
                    System.out.println("doar cele mai mici "  + measureList.get(i).getDateTime().toString());
                    if(k<3){
                        int j = Integer.parseInt(measureList.get(i).getDateTime().toString().substring(11, 13));
                        valueMat[k][j] += measureList.get(i).getValue();
                    }
                    if(i < measureList.size() - 1){
                        if (!measureList.get(i).getDateTime().toString().substring(8, 10).equals(measureList.get(i + 1).getDateTime().toString().substring(8, 10))) {
                            k++;
                        }
                    }

                }
            }

            for(int i =0 ; i<3; i++){
                for (int j = 0 ; j<23; j++){
                    System.out.println("valueMat[" + i + "][" + j + "] = " + valueMat[i][j]);
                }
            }

        };
    }
}
