package ro.tuc.ds2020.dtos;

import lombok.Getter;
import lombok.Setter;

import java.time.LocalDateTime;
import java.util.Date;
import java.util.UUID;

@Getter
@Setter
public class ChartDTO {
    private Date date;
    private UUID clientId;
}
