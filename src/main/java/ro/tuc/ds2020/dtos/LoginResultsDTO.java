package ro.tuc.ds2020.dtos;

import lombok.*;

import java.util.UUID;


@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class LoginResultsDTO {
   private UUID id;
   private String rol;

}
