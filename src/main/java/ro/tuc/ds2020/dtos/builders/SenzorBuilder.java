package ro.tuc.ds2020.dtos.builders;

import ro.tuc.ds2020.dtos.PersonDTO;
import ro.tuc.ds2020.dtos.SenzorDTO;
import ro.tuc.ds2020.entities.Device;
import ro.tuc.ds2020.entities.Person;
import ro.tuc.ds2020.entities.Senzor;

public class SenzorBuilder {
    private SenzorBuilder(){

    }
    public static SenzorDTO toSenzorDTO(Senzor senzor) {
        return new SenzorDTO(senzor.getId(),senzor.getDescription(),senzor.getMaxValue());
    }

    public static Senzor toEntity(SenzorDTO senzorDTO) {
        return new Senzor(null,
                senzorDTO.getDescription(),
                senzorDTO.getMaxValue(),null, null);

    }
}
