package ro.tuc.ds2020.dtos.builders;

import org.springframework.format.datetime.joda.LocalDateTimeParser;
import ro.tuc.ds2020.dtos.DeviceDTO;
import ro.tuc.ds2020.dtos.MeasureDTO;
import ro.tuc.ds2020.entities.Device;
import ro.tuc.ds2020.entities.Measure;

import java.time.LocalDateTime;
import java.time.ZoneId;

public class MeasureBuilder {
    private MeasureBuilder(){


    }
    public static MeasureDTO toMeasureDTO(Measure measure){
        return new MeasureDTO(measure.getId(),
                measure.getDateTime(), measure.getValue());
    }

    public static Measure toEntity(MeasureDTO measureDTO) {

        return new Measure(null,
                measureDTO.getDateTime(),measureDTO.getValue()
        );
    }
}
