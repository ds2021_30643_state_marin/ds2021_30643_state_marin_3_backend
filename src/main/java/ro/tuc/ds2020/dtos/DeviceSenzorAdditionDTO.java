package ro.tuc.ds2020.dtos;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import java.util.UUID;

@Getter
@Setter

public class DeviceSenzorAdditionDTO {
    private UUID deviceId;
    private String description;
    private float maxValue;


}
