package ro.tuc.ds2020.dtos;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import java.util.UUID;

@Getter
@Setter

public class ClientDeviceAdditionDTO {
    private UUID clientId;
    private String description;
    private String address;
    private Float maxEnergyCons;
    private Float averageEnergyCons;


}
