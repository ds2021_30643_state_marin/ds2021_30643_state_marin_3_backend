package ro.tuc.ds2020.dtos;

import java.time.LocalDateTime;
import java.util.Date;
import java.util.UUID;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import java.util.UUID;

@Getter
@Setter

public class SenzorMeasureAdditionDTO {
    private UUID senzorId;
    private LocalDateTime dateTime;
    private Float value;
}
