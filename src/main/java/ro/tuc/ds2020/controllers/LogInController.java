package ro.tuc.ds2020.controllers;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ro.tuc.ds2020.dtos.LoginDTO;
import ro.tuc.ds2020.services.LoginService;

@RestController
@CrossOrigin("*")
public class LogInController {
    private final LoginService loginService;

    public LogInController(LoginService loginService) {
        this.loginService = loginService;
    }

    @PostMapping("/login")
    public ResponseEntity loginReq(@RequestBody LoginDTO dto)  {
        return ResponseEntity.status(HttpStatus.OK).body(loginService.logIn(dto));
    }


}