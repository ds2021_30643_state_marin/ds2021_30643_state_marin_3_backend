package ro.tuc.ds2020.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ro.tuc.ds2020.dtos.*;
import ro.tuc.ds2020.services.ClientService;
import ro.tuc.ds2020.services.DeviceService;
import ro.tuc.ds2020.services.MeasureService;

import javax.validation.Valid;
import java.util.UUID;

@RestController
@RequestMapping("/client")
@CrossOrigin("*")
public class ClientController {
    private final ClientService clientService;
    private final DeviceService deviceService;
    private final MeasureService measureService;

    @Autowired
    public ClientController(ClientService clientService, DeviceService deviceService, MeasureService measureService){
        this.clientService = clientService;
        this.deviceService = deviceService;
        this.measureService = measureService;
    }

    @GetMapping()
    public ResponseEntity findAllClients(){
        return ResponseEntity.status(HttpStatus.OK).body(clientService.findClients());
    }

    @GetMapping(value = "/{id}")
    public ResponseEntity<ClientDTO> getClient(@PathVariable("id") UUID clientId) {
        ClientDTO dto = clientService.findClientById(clientId);
        return new ResponseEntity<>(dto, HttpStatus.OK);
    }

    @GetMapping(value = "/devices/{id}")
    public ResponseEntity findAllDevices(@PathVariable("id") UUID clientId){
        return ResponseEntity.status(HttpStatus.OK).body(deviceService.findDevicesByClientId(clientId));
    }

    @DeleteMapping(value = "/delete/{id}")
    public ResponseEntity deleteClient(@PathVariable("id") UUID clientId){
        return ResponseEntity.status(HttpStatus.OK).body(clientService.deleteClient(clientId));
    }

    @PostMapping()
    public ResponseEntity<UUID> insertClient(@Valid @RequestBody ClientDTO clientDTO) {
        UUID clientId = clientService.insert(clientDTO);
        return new ResponseEntity<>(clientId, HttpStatus.CREATED);
    }
    @PutMapping()
    public ResponseEntity<UUID> updateClient(@Valid @RequestBody ClientDTO clientDTO) {
        UUID clientID = clientService.updateClient(clientDTO);
        return new ResponseEntity<>(clientID, HttpStatus.OK);
    }
    @PutMapping("/update")
    public ResponseEntity updateClientAddDevice(@Valid @RequestBody ClientDeviceAdditionDTO dto){
        UUID deviceId = clientService.updateClientAddingDevice(dto);
        return new ResponseEntity<>(deviceId, HttpStatus.OK);
    }


    @GetMapping(value = "/mySenzor/{id}")
    public ResponseEntity findSnesorByDeviceClientId(@PathVariable("id") UUID clientId){
        return ResponseEntity.status(HttpStatus.OK).body(clientService.findSensorByClientDeviceId(clientId));
    }

    @GetMapping(value = "/myMeasure/{id}")
    public ResponseEntity findMeasurementsByLogClientId(@PathVariable("id") UUID clientId){
        return ResponseEntity.status(HttpStatus.OK).body(measureService.findMeasurementsByClientId(clientId));
    }
    @PostMapping("/totalEnergy")
    public ResponseEntity getTotalEnergyConsumtion(@Valid @RequestBody ClientEnergyDTO clientEnergyDTO){
        return ResponseEntity.status(HttpStatus.OK).body(measureService.getEnergyConspumtionHistory(clientEnergyDTO));
    }

    @PostMapping("/energyToday")
    public ResponseEntity getTotalEnergyConsumtionToday(@Valid @RequestBody ClientEnergyDTO clientEnergyDTO){
        return ResponseEntity.status(HttpStatus.OK).body(measureService.getEnergyConsumptionToday(clientEnergyDTO));
    }
    @PostMapping("/barChart")
    public ResponseEntity test(@Valid @RequestBody ChartDTO barChartClientDTO){
        return ResponseEntity.status(HttpStatus.OK).body(clientService.barCharts(barChartClientDTO));
    }



}
