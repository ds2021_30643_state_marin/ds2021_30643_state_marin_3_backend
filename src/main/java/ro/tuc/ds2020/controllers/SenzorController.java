package ro.tuc.ds2020.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ro.tuc.ds2020.dtos.*;
import ro.tuc.ds2020.services.MeasureService;
import ro.tuc.ds2020.services.SenzorService;

import javax.validation.Valid;
import java.util.UUID;

@RestController
@RequestMapping("/senzor")
@CrossOrigin("*")
public class SenzorController {
    private final MeasureService measureService;
    private final SenzorService senzorService;

    @Autowired
    public SenzorController(MeasureService measureService, SenzorService senzorService){
        this.measureService = measureService;
        this.senzorService = senzorService;
    }

    @GetMapping()
    public ResponseEntity findAllSensors(){
        return ResponseEntity.status(HttpStatus.OK).body(senzorService.findSensors());
    }


    @GetMapping(value = "/{id}")
    public ResponseEntity<SenzorDTO> getSenzor(@PathVariable("id") UUID sensorId) {
        SenzorDTO dto = senzorService.findSenzorById(sensorId);
        return new ResponseEntity<>(dto, HttpStatus.OK);
    }

    @GetMapping(value = "/measure/{id}")
    public ResponseEntity findAllMeasurements(@PathVariable("id") UUID senzorId){
        return ResponseEntity.status(HttpStatus.OK).body(measureService.findMeasurementsBySenzorId(senzorId));
    }
    @DeleteMapping(value = "/delete/{id}")
    public ResponseEntity deleteSenzor(@PathVariable("id") UUID senzorId){
        return ResponseEntity.status(HttpStatus.OK).body(senzorService.deleteSenzor(senzorId));
    }

    @PostMapping()
    public ResponseEntity<UUID> insertSenzor(@Valid @RequestBody SenzorDTO senzorDTO) {
        UUID senzorId= senzorService.insert(senzorDTO);
        return new ResponseEntity<>(senzorId, HttpStatus.CREATED);
    }

    @PutMapping()
    public ResponseEntity<UUID> updateSenzor(@Valid @RequestBody SenzorDTO senzorDTO) {
        UUID senzorId = senzorService.updateSenzor(senzorDTO);
        return new ResponseEntity<>(senzorId, HttpStatus.OK);
    }
    @PutMapping("/updateSenzor")
    public ResponseEntity updateSenzorAddingMeasure(@Valid @RequestBody SenzorMeasureAdditionDTO dto){

        UUID measureId = senzorService.updateSenzorAddingMeasure(dto);
        return new ResponseEntity<>(measureId, HttpStatus.OK);
    }
    @PutMapping("/update")
    public ResponseEntity updateClientAddDevice(@Valid @RequestBody SenzorMeasureAdditionDTO dto){

        UUID measureId = senzorService.updateSenzorAddingMeasure(dto);
        return new ResponseEntity<>(measureId, HttpStatus.OK);
    }
}
