package ro.tuc.ds2020.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ro.tuc.ds2020.dtos.ClientDTO;
import ro.tuc.ds2020.dtos.ClientDeviceAdditionDTO;
import ro.tuc.ds2020.dtos.DeviceDTO;
import ro.tuc.ds2020.dtos.DeviceSenzorAdditionDTO;
import ro.tuc.ds2020.services.DeviceService;
import ro.tuc.ds2020.services.SenzorService;

import javax.validation.Valid;
import java.util.UUID;

@RestController
@RequestMapping("/device")
@CrossOrigin("*")
public class DeviceController {
    private final SenzorService senzorService;
    private final DeviceService deviceService;


    @Autowired
    public DeviceController(SenzorService senzorService, DeviceService deviceService){
        this.senzorService = senzorService;
        this.deviceService = deviceService;
    }

    @GetMapping(value = "/senzor/{id}")
    public ResponseEntity findSenzor(@PathVariable("id") UUID deviceId){
        return ResponseEntity.status(HttpStatus.OK).body(senzorService.findSenzorByDeviceId(deviceId));
    }

    @GetMapping()
    public ResponseEntity findAll(){
        return ResponseEntity.status(HttpStatus.OK).body(deviceService.findAllDevices());
    }

    @DeleteMapping(value = "/delete/{id}")
    public ResponseEntity deleteDevice(@PathVariable("id") UUID deviceId){
        return ResponseEntity.status(HttpStatus.OK).body(deviceService.deleteDevice(deviceId));
    }

    @PostMapping()
    public ResponseEntity<UUID> insertDevice(@Valid @RequestBody DeviceDTO deviceDTO) {
        UUID deviceId = deviceService.insert(deviceDTO);
        return new ResponseEntity<>(deviceId, HttpStatus.CREATED);
    }
    @PutMapping()
    public ResponseEntity<UUID> updateDevice(@Valid @RequestBody DeviceDTO deviceDTO) {
        UUID deviceID = deviceService.updateDevice(deviceDTO);
        return new ResponseEntity<>(deviceID, HttpStatus.OK);
    }
    @PutMapping("/update")
    public ResponseEntity updateDeviceAddSenzor(@Valid @RequestBody DeviceSenzorAdditionDTO dto){
        UUID senzorId = deviceService.updateDeviceAddingSenzor(dto);
        return new ResponseEntity<>(senzorId, HttpStatus.OK);
    }
    @GetMapping(value = "/{id}")
    public ResponseEntity<DeviceDTO> getDevice(@PathVariable("id") UUID deviceId) {
        DeviceDTO dto = deviceService.findDeviceById(deviceId);
        return new ResponseEntity<>(dto, HttpStatus.OK);
    }
}
