package ro.tuc.ds2020.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ro.tuc.ds2020.dtos.MeasureDTO;
import ro.tuc.ds2020.dtos.SensorMeasurementAdditionDTO;
import ro.tuc.ds2020.dtos.SenzorDTO;
import ro.tuc.ds2020.services.MeasureService;
import ro.tuc.ds2020.services.SenzorService;

import javax.validation.Valid;
import java.util.UUID;

@RestController
@RequestMapping("/measure")
@CrossOrigin("*")
public class MeasureController {
    private final MeasureService measureService;
    private final SenzorService senzorService;

    @Autowired
    public MeasureController(MeasureService measureService, SenzorService senzorService){
        this.measureService = measureService;
        this.senzorService = senzorService;
    }

    @DeleteMapping(value = "/delete/{id}")
    public ResponseEntity deleteMeasure(@PathVariable("id") UUID measureId){
        return ResponseEntity.status(HttpStatus.OK).body(measureService.deleteMeasure(measureId));
    }

    @PutMapping("/update")
    public ResponseEntity updateSensorAddMeasurement(@Valid @RequestBody SensorMeasurementAdditionDTO dto){
    UUID sensorId = measureService.updateSensorAddingMeasurement(dto);
    return new ResponseEntity<>(sensorId, HttpStatus.OK);
}
    @PutMapping()
    public ResponseEntity<UUID> updateMeasure(@Valid @RequestBody MeasureDTO measureDTO) {
        UUID measureId = measureService.updateMeasure(measureDTO);
        return new ResponseEntity<>(measureId, HttpStatus.OK);
    }
}
