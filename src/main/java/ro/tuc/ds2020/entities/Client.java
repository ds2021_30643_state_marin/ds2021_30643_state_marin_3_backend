package ro.tuc.ds2020.entities;

import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.Date;
import java.util.List;
import java.util.UUID;

@Entity
@NoArgsConstructor
@Getter
@Setter
public class Client  extends Utilizator {

    private Date birthdate;
    private String address;
    @OneToMany(fetch = FetchType.EAGER, mappedBy = "client")
    private List<Device> deviceList;
    @Builder
    public Client(UUID id, String name, String username, String password, String address, Date birthdate,
                  List<Device> deviceList){
        super(id, name,username,password);
        this.address = address;
        this.birthdate = birthdate;
        this.deviceList = deviceList;
    }

    public String toString(){
        return "id = " + this.getId()+", name = " + this.getName();
    }

}
