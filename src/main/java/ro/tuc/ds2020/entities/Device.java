package ro.tuc.ds2020.entities;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import java.util.UUID;

@Entity
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class Device {
    @Id
    @GeneratedValue(generator = "uuid2")
    @GenericGenerator(name = "uuid2", strategy = "uuid2")
    @Type(type = "uuid-binary")
    private UUID id;
    private String description;
    private String address;
    private Float maxEnergyCons;
    private Float averageEnergyCons;
    @OneToOne
    private Senzor senzor;
    @ManyToOne(fetch =  FetchType.EAGER)
    @JoinColumn(name = "client_id")
    private Client client;


    public Device(String description,String address,Float maxEnergyCons,Float averageEnergyCons,
                  Senzor senzor){
        this.description = description;
        this.address = address;
        this.maxEnergyCons = maxEnergyCons;
        this.averageEnergyCons = averageEnergyCons;
        this.senzor = senzor;
        this.client = null;

    }

}
