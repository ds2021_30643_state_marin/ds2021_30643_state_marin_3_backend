package ro.tuc.ds2020.entities;

import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Entity;
import java.util.UUID;

@Entity
@NoArgsConstructor
@Getter
@Setter
public class Administrator extends Utilizator {
    @Builder
    public Administrator(UUID id, String name, String username, String password){

        super(id, name,username,password);
    }
}
