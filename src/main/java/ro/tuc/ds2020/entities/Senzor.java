package ro.tuc.ds2020.entities;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import java.util.List;
import java.util.UUID;

@Entity
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class Senzor {
    @Id
    @GeneratedValue(generator = "uuid2")
    @GenericGenerator(name = "uuid2", strategy = "uuid2")
    @Type(type = "uuid-binary")
    private UUID id;
    private String description;
    private Float maxValue;
    @OneToMany(fetch = FetchType.EAGER, mappedBy = "senzor")
    private List<Measure> measureList;
    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "device_id")
    private Device device;

    public Senzor(UUID id, String description, Float max_value, List<Measure> measurement) {
        this.id = id;
        this.description = description;
        this.maxValue = max_value;
        this.measureList = measurement;

    }

}
