package ro.tuc.ds2020.services;

import com.googlecode.jsonrpc4j.spring.AutoJsonRpcServiceImpl;
import org.springframework.stereotype.Service;
import ro.tuc.ds2020.entities.Device;
import ro.tuc.ds2020.entities.Measure;
import ro.tuc.ds2020.entities.Senzor;
import ro.tuc.ds2020.repositories.DeviceRepository;
import ro.tuc.ds2020.repositories.SenzorRepository;


import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.*;
@Service
@AutoJsonRpcServiceImpl
public class RPCServiceImpl implements  RPCService{

    private final DeviceRepository deviceRepository;
    private final SenzorRepository senzorRepository;

    public RPCServiceImpl(DeviceRepository deviceRepository, SenzorRepository senzorRepository) {
        this.deviceRepository = deviceRepository;
        this.senzorRepository = senzorRepository;
    }

    @Override
    public Float[][] istoric(UUID idClient, int d, LocalDate date) {
        List<Measure> measureList = new ArrayList<>();
        List<Device> deviceList = deviceRepository.findDevicesById(idClient);
        List<Senzor> senzorList = new ArrayList<>();
        Float[][] valueMat = new Float[d][24];
        for(int i = 0 ; i< d; i++){
            for(int j = 0; j<24; j++){
                valueMat[i][j] = 0f;
            }
        }

        for(Device device: deviceList){
            Senzor senzor = device.getSenzor();
            senzorList.add(senzor);
        }
        for(Senzor senzor:senzorList){
            if (senzor.getMeasureList() != null)
                for(Measure measure : senzor.getMeasureList()){

                    measureList.add(measure);
                }
        }
        Collections.sort(measureList, new Comparator<Measure>() {
            public int compare(Measure o1, Measure o2) {
                return o2.getDateTime().compareTo(o1.getDateTime());
            }
        });
        int k =0;
        for (int i = 0; i< measureList.size(); i++){
            //k contor pana la d
            //j ora
            if(measureList.get(i).getDateTime().getMonth().getValue() < date.getMonthValue() || ((measureList.get(i).getDateTime().getMonth().getValue() == date.getMonthValue()) &&
                    (measureList.get(i).getDateTime().getDayOfMonth() < date.getDayOfMonth()))){

                if(k<d){
                    int j = Integer.parseInt(measureList.get(i).getDateTime().toString().substring(11, 13));
                    valueMat[k][j] += measureList.get(i).getValue();
                }
                if(i < measureList.size() - 1){
                    if (!measureList.get(i).getDateTime().toString().substring(8, 10).equals(measureList.get(i + 1).getDateTime().toString().substring(8, 10))) {
                        k++;
                    }
                }

            }
        }


        return valueMat;
    }

    @Override
    public Float[] media(UUID idClient) {
        List<Measure> measureList = new ArrayList<>();
        List<Device> deviceList = deviceRepository.findDevicesById(idClient);
        List<Senzor> senzorList = new ArrayList<>();
        Float[] valueList = new Float[24];
        for (int i = 0; i<24; i++){
            valueList[i] = 0f;
        }
        for(Device device: deviceList){
            Senzor senzor = device.getSenzor();
            senzorList.add(senzor);
        }
        for(Senzor senzor:senzorList){
            if (senzor.getMeasureList() != null)
                for(Measure measure : senzor.getMeasureList()){

                    measureList.add(measure);
                }
        }

        Collections.sort(measureList, new Comparator<Measure>() {
            public int compare(Measure o1, Measure o2) {
                return o2.getDateTime().compareTo(o1.getDateTime());
            }
        });

        int k = 0;
        for(int i = 0 ; i<measureList.size(); i++){
            if(k<7){
                int j = Integer.parseInt(measureList.get(i).getDateTime().toString().substring(11, 13));
                valueList[j] += measureList.get(i).getValue()/7f;
                if(i<measureList.size()-1){
                    if (!measureList.get(i).getDateTime().toString().substring(8, 10).equals(measureList.get(i + 1).getDateTime().toString().substring(8, 10))) {
                        k++;
                    }
                }
            }
        }

        return valueList;
    }
}
