package ro.tuc.ds2020.services;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ro.tuc.ds2020.controllers.handlers.exceptions.model.ResourceNotFoundException;
import ro.tuc.ds2020.dtos.ClientDTO;
import ro.tuc.ds2020.dtos.DeviceDTO;
import ro.tuc.ds2020.dtos.DeviceSenzorAdditionDTO;
import ro.tuc.ds2020.dtos.builders.ClientBuilder;
import ro.tuc.ds2020.dtos.builders.DeviceBuilder;
import ro.tuc.ds2020.entities.*;
import ro.tuc.ds2020.repositories.DeviceRepository;
import ro.tuc.ds2020.repositories.MeasureRepository;
import ro.tuc.ds2020.repositories.SenzorRepository;

import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
public class DeviceService {
    private final DeviceRepository deviceRepository;
    private final SenzorRepository senzorRepository;
    private final MeasureRepository measureRepository;
    private static final Logger LOGGER = LoggerFactory.getLogger(PersonService.class);
    @Autowired
    public DeviceService(DeviceRepository deviceRepository, SenzorRepository senzorRepository,
                         MeasureRepository measureRepository) {
        this.deviceRepository = deviceRepository;
        this.senzorRepository = senzorRepository;
        this.measureRepository = measureRepository;
    }

    public List<DeviceDTO> findDevicesByClientId(UUID clientId) {
        List<Device> deviceList = deviceRepository.findDevicesById(clientId);

        return deviceList.stream()
                .map(DeviceBuilder::toDeviceDTO)
                .collect(Collectors.toList());
    }

    public List<DeviceDTO> findAllDevices(){
        List<Device> deviceList = deviceRepository.findAll();
        return deviceList.stream()
                .map(DeviceBuilder::toDeviceDTO)
                .collect(Collectors.toList());
    }

    public DeviceDTO deleteDevice(UUID deviceId) {
        Optional<Device> device = deviceRepository.findById(deviceId);
        if(!device.isPresent()){
            LOGGER.error("Person with id {} was not found in db", deviceId);
            throw new ResourceNotFoundException(Person.class.getSimpleName() + " with id: " + deviceId);

        }

        Optional<Senzor> senzor = senzorRepository.findSenzorByDeviceId(device.get().getId());
        deviceRepository.updateSenzorIdAfterDelete(deviceId);
        List<Measure> measureList = measureRepository.findMeasurementsBySenzorId(senzor.get().getId());
        senzor.get().setMeasureList(null);
        senzorRepository.save(senzor.get());
        for(Measure measure : measureList){

            measureRepository.deleteById(measure.getId());
        }
        senzorRepository.deleteById(senzor.get().getId());
        deviceRepository.deleteById(deviceId);
        return null;
    }

    public UUID insert(DeviceDTO deviceDTO) {
        Device device = DeviceBuilder.toEntity(deviceDTO);
        device = deviceRepository.save(device);
        LOGGER.debug("Person with id {} was inserted in db", device.getId());
        return device.getId();
    }

    public UUID updateDevice(DeviceDTO deviceDTO){
        Device device = deviceRepository.findById(deviceDTO.getId()).get();
        device.setDescription(deviceDTO.getDescription());
        device.setAddress(deviceDTO.getAddress());
        device.setMaxEnergyCons(deviceDTO.getMaxEnergyCons());
        device.setAverageEnergyCons(deviceDTO.getAverageEnergyCons());
        deviceRepository.save(device);
        return device.getId();
    }

    public UUID updateDeviceAddingSenzor(DeviceSenzorAdditionDTO dto) {

        Device device = deviceRepository.findById(dto.getDeviceId()).get();
        Senzor senzor = new Senzor();
        senzor.setDescription(dto.getDescription());
        senzor.setMaxValue(dto.getMaxValue());
        senzorRepository.save(senzor);
        if (device.getSenzor() == null) {
            device.setSenzor(senzor);
            deviceRepository.save(device);
        }
        return device.getId();

    }
    public DeviceDTO findDeviceById(UUID id){
        Optional<Device> device = deviceRepository.findById(id);
        if (!device.isPresent()) {
            LOGGER.error("Person with id {} was not found in db", id);
            throw new ResourceNotFoundException(Person.class.getSimpleName() + " with id: " + id);
        }

        return DeviceBuilder.toDeviceDTO(device.get());
    }

}
