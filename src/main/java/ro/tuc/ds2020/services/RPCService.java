package ro.tuc.ds2020.services;

import com.googlecode.jsonrpc4j.JsonRpcMethod;
import com.googlecode.jsonrpc4j.JsonRpcParam;
import com.googlecode.jsonrpc4j.JsonRpcService;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.UUID;

@JsonRpcService("/json")
public interface RPCService {
    @JsonRpcMethod("method")
    Float[][] istoric(@JsonRpcParam(value = "param1") UUID idClient, @JsonRpcParam(value = "param2") int d, @JsonRpcParam(value = "param3") LocalDate date);
    Float[] media(@JsonRpcParam(value = "param1") UUID idClient);
}
