package ro.tuc.ds2020.services;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ro.tuc.ds2020.controllers.handlers.exceptions.model.ResourceNotFoundException;
import ro.tuc.ds2020.dtos.ClientDTO;
import ro.tuc.ds2020.dtos.DeviceDTO;
import ro.tuc.ds2020.dtos.SenzorDTO;
import ro.tuc.ds2020.dtos.SenzorMeasureAdditionDTO;
import ro.tuc.ds2020.dtos.builders.ClientBuilder;
import ro.tuc.ds2020.dtos.builders.DeviceBuilder;
import ro.tuc.ds2020.dtos.builders.PersonBuilder;
import ro.tuc.ds2020.dtos.builders.SenzorBuilder;
import ro.tuc.ds2020.entities.*;
import ro.tuc.ds2020.repositories.DeviceRepository;
import ro.tuc.ds2020.repositories.MeasureRepository;
import ro.tuc.ds2020.repositories.SenzorRepository;
import ro.tuc.ds2020.repositories.UserRepository;

import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
public class SenzorService {
    private static final Logger LOGGER = LoggerFactory.getLogger(PersonService.class);
    private final SenzorRepository senzorRepository;
    private final MeasureRepository measureRepository;
    private final DeviceRepository deviceRepository;

    @Autowired
    public SenzorService(SenzorRepository senzorRepository, MeasureRepository measureRepository,
                         DeviceRepository deviceRepository) {
        this.senzorRepository = senzorRepository;
        this.measureRepository = measureRepository;
        this.deviceRepository = deviceRepository;
    }

    public SenzorDTO findSenzorByDeviceId(UUID deviceId) {
        Optional<Senzor> senzor = senzorRepository.findSenzorByDeviceId(deviceId);

        if (!senzor.isPresent()) {
            LOGGER.error("Person with id {} was not found in db", deviceId);
            throw new ResourceNotFoundException(Senzor.class.getSimpleName() + " with id: " + deviceId);
        }
        return SenzorBuilder.toSenzorDTO(senzor.get());
    }

    public SenzorDTO deleteSenzor(UUID SenzorId) {
        Optional<Senzor> senzor = senzorRepository.findById(SenzorId);
        if(!senzor.isPresent()){
            LOGGER.error("Person with id {} was not found in db", SenzorId);
            throw new ResourceNotFoundException(Person.class.getSimpleName() + " with id: " + SenzorId);
        }
        List<Measure> measureList = measureRepository.findMeasurementsBySenzorId(senzor.get().getId());

        for(Measure measure : measureList){
            measureRepository.deleteById(measure.getId());
        }
        deviceRepository.updateDeviceAfterSenzorDelete(senzor.get().getId());
        senzorRepository.deleteById(senzor.get().getId());

        return null;
    }

    public UUID insert(SenzorDTO senzorDTO) {
        Senzor senzor = SenzorBuilder.toEntity(senzorDTO);
        senzor = senzorRepository.save(senzor);
        LOGGER.debug("Person with id {} was inserted in db", senzor.getId());
        return senzor.getId();
    }

    public UUID updateSenzor(SenzorDTO senzorDTO) {
        Senzor senzor = senzorRepository.findById(senzorDTO.getId()).get();
        senzor.setMaxValue(senzorDTO.getMaxValue());
        senzor.setDescription(senzorDTO.getDescription());

        senzorRepository.save(senzor);
        return senzor.getId();
    }

    public UUID updateSenzorAddingMeasure(SenzorMeasureAdditionDTO dto) {

        Senzor senzor = senzorRepository.findById(dto.getSenzorId()).get();
        Measure measure = new Measure();
        measure.setDateTime(dto.getDateTime());
        measure.setValue(dto.getValue());
        measure.setSenzor(senzor);
        measureRepository.save(measure);
        senzor.getMeasureList().add(measure);
        senzorRepository.save(senzor);
        return measure.getId();

    }


    public List<SenzorDTO> findSensors() {
        List<Senzor> senzorList = senzorRepository.findAll();
        return senzorList.stream()
                .map(SenzorBuilder::toSenzorDTO)
                .collect(Collectors.toList());
    }


    public SenzorDTO findSenzorById(UUID id){
        Optional<Senzor> senzor = senzorRepository.findById(id);
        if (!senzor.isPresent()) {
            LOGGER.error("Person with id {} was not found in db", id);
            throw new ResourceNotFoundException(Person.class.getSimpleName() + " with id: " + id);
        }

        return SenzorBuilder.toSenzorDTO(senzor.get());
    }
}
