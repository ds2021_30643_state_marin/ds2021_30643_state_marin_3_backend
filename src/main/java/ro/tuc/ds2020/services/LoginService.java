package ro.tuc.ds2020.services;

import org.apache.catalina.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ro.tuc.ds2020.controllers.handlers.exceptions.model.ResourceNotFoundException;
import ro.tuc.ds2020.dtos.LoginDTO;
import ro.tuc.ds2020.dtos.LoginResultsDTO;
import ro.tuc.ds2020.entities.Person;
import ro.tuc.ds2020.entities.Utilizator;
import ro.tuc.ds2020.repositories.ClientRepository;
import ro.tuc.ds2020.repositories.UserRepository;

import java.util.Locale;

@Service
public class LoginService {

    private static final Logger LOGGER = LoggerFactory.getLogger(PersonService.class);
    private final UserRepository userRepository;
    @Autowired
    public LoginService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public LoginResultsDTO logIn(LoginDTO dto){
        Utilizator user = userRepository.findByUsername(dto.getUsername());
        if(user == null){
            LOGGER.error("User with username {} was not found in db", dto.getUsername());
            throw new ResourceNotFoundException(User.class.getSimpleName() + " with username: " + dto.getUsername());
        }
        LoginResultsDTO response;
        String role = user.getClass().getSimpleName().toUpperCase();
        if(role.equals("CLIENT")){
            response = LoginResultsDTO.builder().id(user.getId()).rol(role).build();
        }
        else{
            response = LoginResultsDTO.builder().id(user.getId()).rol(role).build();
        }
        if(dto.getPassword().equals(user.getPassword())){
            return response;
        }
        LOGGER.error("Bad credentials");
        throw new ResourceNotFoundException("Bad credentials");
    }

}
