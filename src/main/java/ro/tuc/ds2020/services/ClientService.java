package ro.tuc.ds2020.services;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ro.tuc.ds2020.controllers.handlers.exceptions.model.ResourceNotFoundException;
import ro.tuc.ds2020.dtos.*;
import ro.tuc.ds2020.dtos.builders.ClientBuilder;
import ro.tuc.ds2020.dtos.builders.PersonBuilder;
import ro.tuc.ds2020.dtos.builders.SenzorBuilder;
import ro.tuc.ds2020.entities.*;
import ro.tuc.ds2020.repositories.ClientRepository;
import ro.tuc.ds2020.repositories.DeviceRepository;
import ro.tuc.ds2020.repositories.MeasureRepository;
import ro.tuc.ds2020.repositories.SenzorRepository;

import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.util.*;
import java.util.stream.Collectors;

@Service
public class ClientService {
    private final ClientRepository clientRepository;
    private final DeviceRepository deviceRepository;
    private final SenzorRepository senzorRepository;
    private final MeasureRepository measureRepository;
    private static final Logger LOGGER = LoggerFactory.getLogger(PersonService.class);

    @Autowired
    public ClientService(ClientRepository clientRepository, DeviceRepository deviceRepository, SenzorRepository senzorRepository,
                         MeasureRepository measureRepository) {
        this.clientRepository = clientRepository;
        this.deviceRepository = deviceRepository;
        this.senzorRepository = senzorRepository;
        this.measureRepository = measureRepository;
    }

    public List<ClientDTO> findClients() {
        List<Client> clientList = clientRepository.findAll();
        return clientList.stream()
                .map(ClientBuilder::toClientDTO)
                .collect(Collectors.toList());
    }

    public ClientDTO findClientById(UUID id){
        Optional<Client> client = clientRepository.findById(id);
        if (!client.isPresent()) {
            LOGGER.error("Person with id {} was not found in db", id);
            throw new ResourceNotFoundException(Person.class.getSimpleName() + " with id: " + id);
        }

        return ClientBuilder.toClientDTO(client.get());
    }

    public Client deleteClient(UUID clientId) {
        Optional<Client> client = clientRepository.findById(clientId);

        if(!client.isPresent()){
            LOGGER.error("Person with id {} was not found in db", clientId);
            throw new ResourceNotFoundException(Person.class.getSimpleName() + " with id: " + clientId);

        }
        //List<Device> deviceList = deviceRepository.findDevicesById(clientId);
       // for(Device device : deviceList){
            deviceRepository.updateDeviceIdAfterDelete(clientId);

        //}

        clientRepository.deleteById(clientId);
        return null;
    }

    public UUID insert(ClientDTO clientDTO) {
        Client client = ClientBuilder.toEntity(clientDTO);
        client = clientRepository.save(client);
        LOGGER.debug("Person with id {} was inserted in db", client.getId());
        return client.getId();
    }

    public UUID updateClient(ClientDTO clientDTO){
        Client client = clientRepository.findById(clientDTO.getId()).get();
        client.setName(clientDTO.getName());
        client.setBirthdate(clientDTO.getBirthdate());
        client.setAddress(clientDTO.getAddress());
        client.setUsername(clientDTO.getUsername());
        client.setPassword(clientDTO.getPassword());
        clientRepository.save(client);
        return client.getId();
    }

    public UUID updateClientAddingDevice(ClientDeviceAdditionDTO dto) {

        Client client = clientRepository.findById(dto.getClientId()).get();
        Device device = new Device();
        device.setClient(client);
        device.setDescription(dto.getDescription());
        device.setAddress(dto.getAddress());
        device.setMaxEnergyCons(dto.getMaxEnergyCons());
        device.setAverageEnergyCons(dto.getAverageEnergyCons());
        deviceRepository.save(device);
        client.getDeviceList().add(device);
        clientRepository.save(client);
        return client.getId();

    }

    public List<Senzor> findSenzorsByClientId(UUID clientId){
        List<Senzor> senzorList = new ArrayList<>();
        List<Device> deviceList = deviceRepository.findDevicesById(clientId);
        for(Device device : deviceList){
            Senzor senzor = device.getSenzor();
            senzorList.add(senzor);
        }

        return senzorList;
    }

    public List<SenzorDTO> findSensorByClientDeviceId(UUID clientId) {
        List<Device> deviceList = deviceRepository.findDevicesById(clientId);
        List<Senzor> sensorList = new ArrayList<>();
        if(deviceList != null) {
            for (Device device : deviceList) {
                Optional<Senzor> sensor = senzorRepository.findSenzorByDeviceId(device.getId());
                if(sensor.isPresent()) {
                    sensorList.add(sensor.get());
                }

            }
        }
        return sensorList.stream()
                .map(SenzorBuilder::toSenzorDTO)
                .collect(Collectors.toList());

    }

    public List<Measure> findMeasureByClientId(UUID clientId){
        List<Measure> measureList = new ArrayList<>();
        List<Senzor> senzorList = this.findSenzorsByClientId(clientId);
        for(Senzor senzor : senzorList){
            for(Measure measure : senzor.getMeasureList()){
                measureList.add(measure);
            }
        }
        return measureList;
    }
    public List<Measure> findMeasureByClientId2(UUID clientId){
        List<Device> deviceList = deviceRepository.findDevicesById(clientId);
        List<Measure> measureList = new ArrayList<>();
        List<Senzor> senzorList = new ArrayList<>();
        for(Device device : deviceList){
            Senzor senzor = device.getSenzor();
            senzorList.add(senzor);
        }
        for(Senzor senzor : senzorList){
            for(Measure measure : senzor.getMeasureList()){
                measureList.add(measure);
            }
        }

        return measureList;
    }

    public Float[] barCharts(ChartDTO chartDTO) {
        Float[] valueList = new Float[24];
        for (int i = 0; i < 24; i++) {
            valueList[i] = 0f;
        }
        int x = 0;
        List<Device> deviceList = deviceRepository.findDevicesById(chartDTO.getClientId());
        List<Measure> measureList = new ArrayList<>();
        List<Senzor> senzorList = new ArrayList<>();
        for(Device device : deviceList){
            Senzor senzor = device.getSenzor();
            senzorList.add(senzor);
        }

        for(Senzor senzor : senzorList){
            if (senzor.getMeasureList() != null)
            for(Measure measure : senzor.getMeasureList()){
                measureList.add(measure);
            }
        }
        for (Measure measure : measureList) {
            x++;
            SimpleDateFormat simpleformat = new SimpleDateFormat("yyyy-MM-dd hh:mm:s");
           String date = simpleformat.format(chartDTO.getDate());

            if(measure.getDateTime().toString().substring(0,10).equals(date.substring(0,10)) ){
                int i = Integer.parseInt(measure.getDateTime().toString().substring(11,13));

                valueList[i] += measure.getValue();
            }
        }

        return valueList;
    }



    }



