package ro.tuc.ds2020.services;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ro.tuc.ds2020.controllers.handlers.exceptions.model.ResourceNotFoundException;
import ro.tuc.ds2020.dtos.ClientEnergyDTO;
import ro.tuc.ds2020.dtos.DeviceDTO;
import ro.tuc.ds2020.dtos.MeasureDTO;
import ro.tuc.ds2020.dtos.SensorMeasurementAdditionDTO;
import ro.tuc.ds2020.dtos.builders.DeviceBuilder;
import ro.tuc.ds2020.dtos.builders.MeasureBuilder;
import ro.tuc.ds2020.dtos.builders.SenzorBuilder;
import ro.tuc.ds2020.entities.*;
import ro.tuc.ds2020.repositories.DeviceRepository;
import ro.tuc.ds2020.repositories.MeasureRepository;
import ro.tuc.ds2020.repositories.SenzorRepository;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.stream.Collectors;

@Service
public class MeasureService {
    private final MeasureRepository measureRepository;
    private final DeviceRepository deviceRepository;
    private final SenzorRepository senzorRepository;
    private static final Logger LOGGER = LoggerFactory.getLogger(PersonService.class);
    @Autowired
    public MeasureService(MeasureRepository measureRepository, DeviceRepository deviceRepository, SenzorRepository senzorRepository) {
        this.measureRepository = measureRepository;
        this.deviceRepository = deviceRepository;
        this.senzorRepository = senzorRepository;
    }

    public List<MeasureDTO> findMeasurementsBySenzorId(UUID senzorId) {
        List<Measure> measureList = measureRepository.findMeasurementsBySenzorId(senzorId);
        return measureList.stream()
                .map(MeasureBuilder::toMeasureDTO)
                .collect(Collectors.toList());
    }

    public MeasureDTO deleteMeasure(UUID measureId) {
        Optional<Measure> measure = measureRepository.findById(measureId);
        if(!measure.isPresent()){
            LOGGER.error("Person with id {} was not found in db", measureId);
            throw new ResourceNotFoundException(Person.class.getSimpleName() + " with id: " + measureId);
        }
        measureRepository.deleteById(measureId);
        return null;
    }


    public UUID updateSensorAddingMeasurement(SensorMeasurementAdditionDTO dto) {
        Senzor sensor = senzorRepository.findById(dto.getSensorId()).get();
        Measure measurement = new Measure();
        measurement.setSenzor(sensor);
        String concate = dto.getDate() + " " + dto.getTime();
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");
        LocalDateTime dateTime = LocalDateTime.parse(concate, formatter);
        measurement.setDateTime(dateTime);
        measurement.setValue(dto.getValue());
        measureRepository.save(measurement);
        return sensor.getId();

    }
    public UUID updateMeasure(MeasureDTO measureDTO) {
        Measure measure = measureRepository.findById(measureDTO.getSenzorId()).get();
        measure.setValue(measureDTO.getValue());
        measure.setDateTime(measureDTO.getDateTime());
        measureRepository.save(measure);
        return measure.getId();
    }

    public List<MeasureDTO> findMeasurementsByClientId(UUID clientId) {
        List<Device> deviceList = deviceRepository.findDevicesById(clientId);
        List<Senzor> sensorList = new ArrayList<>();
        List<Measure> measurementList = new ArrayList<>();
        if (deviceList != null) {
            for (Device device : deviceList) {
                Optional<Senzor> sensor = senzorRepository.findSenzorByDeviceId(device.getId());
                if (sensor.isPresent()) {
                    sensorList.add(sensor.get());
                }

            }
            for (Senzor sensor : sensorList) {
                List<Measure> measurements = measureRepository.findMeasurementsBySenzorId(sensor.getId());
                if (measurements != null) {
                    for (Measure measurement : measurements) {
                        measurementList.add(measurement);
                    }
                }
            }
        }
        return measurementList.stream()
                .map(MeasureBuilder::toMeasureDTO)
                .collect(Collectors.toList());
    }
    public Float getEnergyConsumptionToday(ClientEnergyDTO clientEnergyDTO) {
        List<Device> deviceList = deviceRepository.findDevicesById(clientEnergyDTO.getClientId());
        List<Senzor> sensorList = new ArrayList<>();
        List<Float> valueList = new ArrayList<>();
        Float suma = 0f;
        Float sumaFinala = 0f;
        if (deviceList != null) {
            for (Device device : deviceList) {
                Optional<Senzor> sensor = senzorRepository.findSenzorByDeviceId(device.getId());
                if (sensor.isPresent()) {
                    sensorList.add(sensor.get());
                }
            }
            for(Senzor sensor:sensorList){
                sumaFinala = 0f;
                List<Measure> measurements = measureRepository.findMeasurementsBySenzorId(sensor.getId());
                for (Measure measurement : measurements) {
                    String today = LocalDateTime.now().toString().substring(0, 10);
                    String date = measurement.getDateTime().toString().substring(0, 10);
                    if (date.equals(today)) {
                        valueList.add(measurement.getValue());
                    }
                }
                Collections.sort(valueList, Collections.reverseOrder());
                for(int i =0; i<valueList.size()-1; i++){
                    suma += valueList.get(i) - valueList.get(i+1);
                }
                sumaFinala += suma;
            }

        }
        return sumaFinala;
    }
    public Float getEnergyConspumtionHistory(ClientEnergyDTO clientEnergyDTO){
        List<Device> deviceList = deviceRepository.findDevicesById(clientEnergyDTO.getClientId());
        List<Senzor> sensorList = new ArrayList<>();
        List<Float> valueList = new ArrayList<>();
        Float suma = 0f;
        if(deviceList != null) {
            for(Device device:deviceList){
                Optional<Senzor> sensor = senzorRepository.findSenzorByDeviceId(device.getId());
                if (sensor.isPresent()) {
                    sensorList.add(sensor.get());
                }

            }
            for (Senzor sensor: sensorList){
                List<Measure> measurements = measureRepository.findMeasurementsBySenzorId(sensor.getId());
                for (Measure measurement : measurements) {                     valueList.add(measurement.getValue());
                    valueList.add(measurement.getValue());
                }
                Collections.sort(valueList, Collections.reverseOrder());
                suma += valueList.get(0);

            }
        }
        return suma;
    }
}
