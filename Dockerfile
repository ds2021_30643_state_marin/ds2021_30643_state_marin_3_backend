FROM maven:3.6.3-jdk-11 AS builder
COPY ./src/ /root/src
COPY ./pom.xml /root/
COPY ./checkstyle.xml /root/
WORKDIR /root
RUN mvn package
RUN java -Djarmode=layertools -jar /root/target/ds-2020-0.0.1-SNAPSHOT.jar list
RUN java -Djarmode=layertools -jar /root/target/ds-2020-0.0.1-SNAPSHOT.jar extract
RUN ls -l /root

FROM openjdk:11.0.6-jre

ENV TZ=UTC
ENV DB_IP=ec2-34-253-116-145.eu-west-1.compute.amazonaws.com
ENV DB_PORT=5432
ENV DB_USER=biefuauizgwunr
ENV DB_PASSWORD=c23f93dca2cf23171080273c2f36d909c3d7c421ae1d7f2ee7b1c68267538808
ENV DB_DBNAME=dc0iimm747brhh


COPY --from=builder /root/dependencies/ ./
COPY --from=builder /root/snapshot-dependencies/ ./
RUN sleep 10
COPY --from=builder /root/spring-boot-loader/ ./
COPY --from=builder /root/application/ ./
ENTRYPOINT ["java", "org.springframework.boot.loader.JarLauncher","-XX:+UseContainerSupport -XX:+UnlockExperimentalVMOptions -XX:+UseCGroupMemoryLimitForHeap -XX:MaxRAMFraction=1 -Xms128m -Xmx128m -XX:+UseG1GC -XX:+UseSerialGC -Xss128k -XX:MaxRAM=2048m"]